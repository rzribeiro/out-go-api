var knex = require('knex')

knex_conf = {
    client: 'sqlite3',
    connection: {
        filename: './outgo.db'
    }
};

module.exports = knex(knex_conf);