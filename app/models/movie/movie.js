const knex = require('../../config/knexfile');
const category = require('../category/category');


module.exports = {
    getAll(category, name) {
            let query = knex.select();
            
            if(category!=undefined){
                query = query.where('category', parseInt(category));
            }
            
            if(name!=undefined){
                query = query.whereRaw(`LOWER(title) LIKE ?`, [`%${name.toLowerCase()}%`]);
            }

            return query.table('movies');
    },
    getById(id) {
        return knex.where({'id' : id})
                   .select()
                   .table('movies')
    },
    insertOne(movie){
        return new Promise(function(resolve, reject) {
            let promise = category.findOrInsert(movie.categoryName);
            promise.then(
                function(categoryId){
                    movie.category = categoryId;
                    delete movie.categoryName;
                    resolve(knex.insert(movie)
                                .table('movies'));
                }
            )
        });
    },
    update(id, movie){
        return knex.where({'id': id})
                   .update(movie)
                   .table('movies');
    }
}