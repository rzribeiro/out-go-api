const knex = require('../../config/knexfile');

module.exports = {
    getAll() {
        return knex.select()
                   .table('categories');
    },
    getById(id) {
        return knex.where({'id' : id})
                   .select()
                   .table('categories')
    },
    insertOne(category){
        return knex.insert(category)
                   .table('categories');
    },
    findOrInsert(name){
        return new Promise(function(resolve, reject) {
            knex.whereRaw(`LOWER(name) LIKE ?`, [`${name.toLowerCase()}`])
                .select()
                .table('categories')
                .then((category) => {
                    if(category.length>0){
                        resolve(category[0].id);
                    }
                    else {
                        knex.insert({ "name": name })
                            .table('categories')
                            .then((id) => { resolve(id[0]); });
                    }
                });
        });
    },
    update(id, category){
        return knex.where({'id': id})
                   .update(category)
                   .table('categories');
    }
}