var express = require('express');
var router = express.Router();
var category = require('../models/category/category')

router.get('/', function(req, res, next){
    var promise = category.getAll();
    promise.then(function(movies){
        res.json(movies);
    });
});

router.post('/', function(req, res, next){
    var promise = category.insertOne(req.body);
    promise.then(function(movieId){
        res.json(movieId);
    });
});

router.put('/:id', function(req, res, next){
    var promise = category.update(req.params.id, req.body);
    promise.then(function(movie){
        res.json(movie);
    });
});

module.exports = router;