var express = require('express');
var router = express.Router();
var movie = require('../models/movie/movie')

router.get('/', function(req, res, next){
    let category = req.query.category;
    let name = req.query.name;
    var promise = movie.getAll(category, name);
    promise.then(function(movies){
        res.json(movies);
    });
});

router.post('/', function(req, res, next){
    var promise = movie.insertOne(req.body);
    promise.then(function(movieId){
        res.json(movieId);
    });
});

router.put('/:id', function(req, res, next){
    var promise = movie.update(req.params.id, req.body);
    promise.then(function(movie){
        res.json(movie);
    });
});

module.exports = router;