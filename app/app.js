var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var movies = require('./routes/movies');
var categories = require('./routes/categories');

var app = express();

app.use(bodyParser({limit: '50mb'}));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

app.use('/api/v1/movies', movies);
app.use('/api/v1/categories', categories);

module.exports = app;
